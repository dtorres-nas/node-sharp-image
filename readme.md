# Bulk Image Conversion
## Node.js script using [Sharp](https://github.com/lovell/sharp) and [Azure JS SDK](https://docs.microsoft.com/en-us/javascript/api/@azure/storage-blob/?view=azure-node-latest)
Easily convert image files using Sharp image processing library, original image files sourced from Azure storage.

```bash
git clone https://bitbucket.org/dtorres-nas/node-sharp-image.git
cd node-sharp-image
npm install
```

To run the script, provide the client folder you wish to target in the command line:
```bash
node index.mjs {TenantName}/Images {cache | no-cache}
```

You'll then see a message like this:
```markdown
Script running in folder {TenantName}/Images...
Total number of Blobs: 433
Number of blobs with incompatible filetypes: 0
Number of blobs eligible for conversion (jpg, png): 217
Number of blobs already converted to webp: 216
Number of blobs not yet converted to webp: 2
Blobs being converted...
Converted {TenantName}/Images/photo1.webp and added to queue
Converted {TenantName}/Images/photo2.webp and added to queue
Pushing queued blobs to Azure...
{TenantName}/Images/photo1.webp successfully uploaded to Azure storage
{TenantName}/Images/photo2.webp successfully uploaded to Azure storage
```

If your node command is incorrect, or there is a problem with the Tenant folder you've provided, you will see one of these error messages:
```markdown
ERROR: Please provide a folder name in the format "node index.mjs {TenantName}/Images cache"

ERROR: Please provide a caching strategy in the format "node index.mjs {TenantName}/Images {cache | no-cache}"

ERROR: No blobs found for the folder "{TenantName}/Images"
```

### Why use this script?
When using images on modern websites, we sometimes want to send users a ```.webp``` file instead of a standard ```.jpg``` or ```.png``` file.  Usually, this would be converted and cached on-the-fly by a third-party image service.  However, when we're not using such services, this can be a major pain point if we need to convert large numbers of images to ```.webp``` format.

### Why is Azure involved?
Our current data storage solution is Azure CDN.  Azure provides a Node.js SDK to access and manipulate data on the platform.  This means that we can pull images directly from Azure, manipulate them, and reupload them programmatically.  This reduces the time and local storage space needed to manually convert files.

The flag you provide in your node command will determine if Azure sets cache-control headers on the new files you upload.  This script will not overwrite existing cache-control headers, it's only purpose is writing new files to Azure.

### What does Sharp do?
Sharp is an image processing module for Node.  It has all kinds of features, but the one we're focusing on in this repo is conversion to ```.webp```.  Sharp supports the same data-types that Azure Storage does.  They also provide resizing, cropping, metadata, and more to be explored in a more full-featured in-house image workflow.

### Downsides
Since this is a bulk tool, the specific options when converting to WebP are the same for every file.  This can work against you if you want finer control over the quality for each file.  By default, WebP files are saved at 80% quality for JPEGs and 100% quality (lossless) for PNG files.  You can change the variables in ```index.mjs``` within the ``` switch (fileType) {...}``` section to modify these settings. There you can conditionally change the quality options depending on filename or other parameters, if specific settings are required.

### Future
This script runs locally, only converts files to one format and still requires us to manually convert and store files for cacheing.  In the future, this script could be included in an in-house image optimization service, ideally with automatic conversion, caching, size transformations and sending users appropriate image types (similar to [ImageKit.io](https://imagekit.io/) or [Cloudinary](https://cloudinary.com/))