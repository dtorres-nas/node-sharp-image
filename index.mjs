// import fs from "fs";
import sharp from "sharp";
import { Buffer } from "buffer";
import {
  BlobServiceClient,
  BlobClient
} from "@azure/storage-blob";

const args = process.argv.slice(2);

async function sharpBufferToWebp(imgBuffer, options) {
  const formatOptions = options || {};
  return sharp(imgBuffer).toFormat("webp", formatOptions).toBuffer();
}

async function main() {
  // This sets up the script to only run if the user provides a folder name
  if (args.length === 0) {
    console.error(`ERROR: Please provide a folder name in the format "node index.mjs {TenantName}/Images cache"`);
    process.exit(1);
  } else if (args.length > 0 && !args[0].match(/.+\/Images/)) {
    console.error(`ERROR: Please provide a folder name in the format "node index.mjs {TenantName}/Images cache"`);
    process.exit(1);
  } else if (args.length > 0) {
    if (args[1] === undefined) {
      console.error(`ERROR: Please provide a caching strategy in the format "node index.mjs {TenantName}/Images {cache | no-cache}"`);
      process.exit(1);
    } else if (!args[1].match(/cache/) && !args[1].match(/no-cache/)) {
      console.error(`ERROR: Please provide a caching strategy in the format "node index.mjs {TenantName}/Images {cache | no-cache}"`);
      process.exit(1);
    }
  }
  const folderName = args[0];
  const shouldFilesBeCached = args[1] === "cache";

  // Connect with Azure Storage
  const AZURE_STORAGE_CONNECTION_STRING =
    process.env.AZURE_STORAGE_CONNECTION_STRING;
  const blobServiceClient = BlobServiceClient.fromConnectionString(
    AZURE_STORAGE_CONNECTION_STRING
  );
  const containerName = "activatedata";
  const containerClient = blobServiceClient.getContainerClient(containerName);
  // End Connect with Azure Storage

  // Get image from Azure as Buffer, convert to new filetype using Sharp, push to Queue List
  const queueList = new Array();
  async function addBufferToList(originalBlobName, newBlobName, options) {
    // Azure SDK BlobClient to access Buffer data
    const targetBlobClient = new BlobClient(
      AZURE_STORAGE_CONNECTION_STRING,
      containerName,
      originalBlobName
    );
    // Get raw IMG data from Azure - imgBuffer => Promise<Buffer>
    const imgBuffer = await targetBlobClient.downloadToBuffer();
    console.log(`Converted ${newBlobName} and added to queue`);
    queueList.push({
      name: newBlobName,
      buffer: await sharpBufferToWebp(imgBuffer, options),
    });
  };

  // Upload WebP buffer to Azure Blob storage
  async function uploadBufferToAzure(blobName, buffer) {
    containerClient.uploadBlockBlob(
      blobName,
      buffer,
      Buffer.byteLength(buffer),
      {
        blobHTTPHeaders: {
          blobContentType: "image/webp",
          // Optional cache control settings
          blobCacheControl: shouldFilesBeCached ? "public, max-age=15552000" : "",
        },
      }
    );
  };


  async function convertReuploadAzureFilesAsWebP() {
    // For loop on files found in "folderName", save list of converted Buffer files, reupload
    const blobs = [];
    for await (const blob of containerClient.listBlobsFlat({ prefix: folderName })) {

      const blobName = blob.name;
      const fileType = blob.properties.contentType;
      const options = {};
      let canBeConverted = false;

      switch (fileType) {
        // Apply high quality options for PNGs (optional)
        case "image/png":
          options.quality = 100;
          options.lossless = true;
          canBeConverted = true;
          break;
        // Apply quality options for JPGs (optional, 100 recommended for previously compressed images)
        case "image/jpeg":
          options.quality = 80;
          canBeConverted = true;
          break;
        // Webps Can be Converted, catching
        case "image/webp":
          canBeConverted = true;
          break;
      };

      blobs.push({
        blobName,
        options,
        fileType,
        canBeConverted
      });

    };
    if (blobs.length > 0) {
      console.log("Total number of Blobs:", blobs.length);
    } else {
      console.error(`ERROR: No blobs found for the folder "${folderName}"`);
      process.exit(1);
    }

    const blobsIncompatible = blobs.filter((blob) => !blob.canBeConverted);

    const blobsAsJpgPng = blobs.filter((blob) => {
      if (
        (blob.fileType === "image/jpeg") ||
        (blob.fileType === "image/png")
      ) {
        return true;
      }
    });

    const blobsAsWebp = blobs.filter((blob) => {
      if (blob.fileType === "image/webp") {
        return true;
      }
    });

    const blobsNotYetConverted = blobsAsJpgPng.filter((blob) => {
      const convertedFileName = `${blob.blobName.substring(0, blob.blobName.lastIndexOf("."))}.webp`;
      const shouldBlobBeConverted = !blobsAsWebp.some((blob) => blob.blobName.includes(convertedFileName));
      return shouldBlobBeConverted;
    });

    console.log("Number of blobs with incompatible filetypes:", blobsIncompatible.length);
    console.log("Number of blobs eligible for conversion (jpg, png):", blobsAsJpgPng.length);
    console.log("Number of blobs already converted to webp:", blobsAsWebp.length);
    console.log("Number of blobs not yet converted to webp:", blobsNotYetConverted.length);

    if (blobsNotYetConverted.length > 0) {
        console.log(
          "Blobs being converted..."
        );
        for (const blobToBeConverted of blobsNotYetConverted) {
          const convertedFileName = `${blobToBeConverted.blobName.substring(0, blobToBeConverted.blobName.lastIndexOf("."))}.webp`;
          await addBufferToList(blobToBeConverted.blobName, convertedFileName, blobToBeConverted.options);
        };
        console.log("Pushing queued blobs to Azure...");
        for (const newBlob of queueList) {
          uploadBufferToAzure(newBlob.name, newBlob.buffer)
            .then(() => {
              console.log(`${newBlob.name} successfully uploaded to Azure storage`);
            });
        };
    } else {
      console.log(
        "No blobs to be converted."
      )
    };
  };

  convertReuploadAzureFilesAsWebP();
}

main()
  .then(() => console.log(`Script running in folder ${args[0]}...`))
  .catch((ex) => console.log(ex.message));
